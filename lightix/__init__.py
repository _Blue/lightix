#! /usr/bin/python3

import yaml
import sys
import traceback
import click
from wifi import Cell
from datetime import datetime, timezone, timedelta
from time import sleep, time
from threading import Thread, Lock


running = True
lock = Lock()
polled_networks = {}

def sleep_until(ts: datetime):
    end = ts.astimezone(timezone.utc).timestamp()

    while True:
        now = time()
        diff = end - now

        if diff <= 0:
            return
        else:
            sleep(diff / 2)


def poll_networks(config: dict):
    global running
    global lock
    global polled_networks

    while running:
        next_poll = datetime.now() + timedelta(milliseconds=config['wifi_poll_rate_ms'])

        try:
            networks = Cell.all(config['wifi_interface'])

            matched_networks = {e.ssid: e.signal for e in networks}

            with lock:
                polled_networks = matched_networks
        except:
            traceback.print_exc()
        sleep_until(next_poll)

    print('Network poll loop exiting')

def update_state(state: dict, networks: dict, weight: float):
    for ssid, signal in state.items():
        current_signal = networks.get(ssid, None)

        # The higher the signal, the stronger
        if current_signal is None or current_signal < signal:
            state[ssid] = max(signal - weight, current_signal or -100)
        elif current_signal > signal:
            state[ssid] = min(signal + weight, current_signal)

def color(value: dict):
    return value['red'], value['green'], value['blue']

def add_contribution(left, right, factor: float):
    return round(min(255, left[0] + right[0] * factor)), round(min(255, left[1] + right[1] * factor)), round(min(255, left[2] + right[2] * factor))

def compute_color(networks: dict, base_color, state: dict):
    output = base_color

    for ssid, signal in state.items():
        network_config = networks[ssid]

        if signal <= network_config['range']['floor']:
            contribution = 0
        elif signal >= network_config['range']['ceiling']:
            contribution = 1
        else:
            contribution = abs((network_config['range']['floor'] - signal) / float(network_config['range']['ceiling'] - network_config['range']['floor']))

        output = add_contribution(output, color(network_config['color']), contribution)

    return output

def update_colors(network_config: dict, base_color, weight: float, refresh_rate_ms: int, verbose: bool):
    global lock
    global polled_networks

    state = {e: network_config[e]['range']['floor'] for e in network_config}

    while running:
        ts = datetime.now()
        next_poll = ts + timedelta(milliseconds=refresh_rate_ms)

        with lock:
            networks = polled_networks

        update_state(state, networks, weight)
        if verbose:
            print(f'State: {state}')

        print(compute_color(network_config, base_color, state))

        sleep_until(next_poll)

@click.group()
def cli():
    pass


@cli.command()
@click.argument('config_path', default='/etc/lightix/lightix.yml')
@click.option('--verbose', '-v', is_flag=True)
@click.option('--debug', '-d', is_flag=True)
def main(config_path: str, verbose: str, debug: str):
    global running

    try:
        with open(config_path) as fd:
            config = yaml.safe_load(fd)

        networks_config = {e['ssid']: e for e in config['networks']}

        thread = Thread(target=poll_networks, args=(config,))
        thread.start()

        update_colors(networks_config, color(config['color']), config['weight'], config['refresh_rate_ms'], verbose)
    except:
        traceback.print_exc()

        if debug:
            import pdb
            pdb.post_mortem()
    finally:
        running = False


if __name__ == '__main__':
    main()
