from setuptools import setup, find_packages

setup(
    name="lightix",
    version="1",
    packages=find_packages(),
    include_package_data=True,
    entry_points={
        "console_scripts": ["lightix = lightix:main"]
        },
    install_requires=[
        'PyYAML==6.0',
        'click==8.0.4',
        'wifi==0.3.8'
        ]
    )

